#!/bin/sh

source environment.sh

kops get cluster ${CLUSTER_NAME} -o yaml > ${CLUSTER_NAME}.yaml
echo " " >> ${CLUSTER_NAME}.yaml
echo "---" >>${CLUSTER_NAME}.yaml
echo " " >> ${CLUSTER_NAME}.yaml
kops get ig --name ${CLUSTER_NAME} -o yaml >> ${CLUSTER_NAME}.yaml
