#!/bin/sh

source environment.sh

echo " \
kops replace -f ../cluster/$CLUSTER_NAME.yaml --state=$KOPS_STATE_STORE \
kops update cluster $CLUSTER_NAME --yes --state=$KOPS_STATE_STORE --yes \
"
kops replace -f ../cluster/${CLUSTER_NAME}.yaml --state=${KOPS_STATE_STORE}
kops update cluster ${CLUSTER_NAME} --yes --state=${KOPS_STATE_STORE} --yes
